import numpy as np
from gensim.models import FastText, KeyedVectors


class DomainEmbeddingsCutter:

    @staticmethod
    def limit(embeddings_path, words_path, output_path):
        units = DomainEmbeddingsCutter.__load_units(words_path)
        domains_vector = DomainEmbeddingsCutter.__load_vectors(embeddings_path, units)
        DomainEmbeddingsSaver.save(domains_vector, output_path)

    @staticmethod
    def __load_units(path):
        units_result = []
        with open(path, 'r') as file:
            for line in file:
                line = line.rstrip()
                parts = line.split('=')
                word = parts[0]
                if len(parts) == 2:
                    domain = parts[1]
                else:
                    domain = -1
                units_result.append((word, int(domain)))
        return units_result

    @staticmethod
    def __load_vectors(path, units):
        model, is_fast_text_format = DomainEmbeddingsCutter.__load_model(path)
        domains_vectors = {}
        for unit in units:
            word = unit[0]
            domain = unit[1]
            if word in model.wv.vocab and DomainEmbeddingsCutter.__is_multiword(word):
                vector = DomainEmbeddingsCutter.__generate_multiword(word, model)
            else:
                try:
                    vector = model.wv[word]
                except:
                    vector = None

            if vector is not None:
                if domain not in domains_vectors:
                    domains_vectors[domain] = {}
                domains_vectors[domain][word] = vector

        return domains_vectors

    # TODO: zrobić część wspólną z cutter

    @staticmethod
    def __load_model(path):

        fastext_format = path.endswith('.bin')
        model = FastText.load_fasttext_format(path, encoding='ISO-8859-1') if fastext_format \
            else KeyedVectors.load_word2vec_format(path, binary=False)
        return model, fastext_format

    @staticmethod
    def __is_multiword(word):
        return '_' in word or '-' in word

    # TODO: zrobić klasę generowania wielowyrazowców
    @staticmethod
    def __generate_multiword(word, model):
        vectors = []
        parts = word.split('_')
        vectors = [model.wv[part] for part in parts if part in model.wv.vocab]
        if len(vectors) != len(parts):
            return None
        return np.average(vectors, axis=0)


class DomainEmbeddingsSaver:

    @staticmethod
    def save(domains_vectors, path):
        with open(path, 'w') as file:
            for domain, vectors in domains_vectors.items():
                file.write('#D {}\n'.format(str(domain)))
                for word, vector in vectors.items():
                    vector_str = ' '.join(str(x) for x in vector)
                    file.write('{} {}\n'.format(word, vector_str))


class DomainEmbeddingsLoader:

    @staticmethod
    def load(path):
        domains_vectors = {}
        with open(path, 'r') as file:
            current_domain = -1
            vectors = {}
            for line in file:
                line = line.rstrip()
                if DomainEmbeddingsLoader.__is_domain_line(line):
                    if current_domain != -1:
                        DomainEmbeddingsLoader.__insert_model(current_domain, vectors, domains_vectors)
                        vectors = {}
                    current_domain = int(line.split(' ')[1])
                else:
                    DomainEmbeddingsLoader.__insert_vector(line, vectors)
            DomainEmbeddingsLoader.__insert_model(current_domain, vectors, domains_vectors)
        return domains_vectors

    @staticmethod
    def __insert_vector(line, vectors):
        unit, vector_str = line.split(' ', 1)
        vector = np.fromstring(vector_str, sep=' ')
        vectors[unit] = vector

    @staticmethod
    def __is_domain_line(line):
        return line.startswith('#D')

    @staticmethod
    def __insert_model(domain, vectors, domains_vectors):
        VECTOR_SIZE = 300
        model = KeyedVectors(VECTOR_SIZE)
        words = list(vectors.keys())
        vectors_list = list(vectors.values())
        model.add(words, vectors_list)
        domains_vectors[domain] = model
