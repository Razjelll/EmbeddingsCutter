import io

import numpy as np
from gensim.models import FastText, KeyedVectors


class Cutter:
    # TODO: pozmieniać nazwy
    @staticmethod
    def limit(embeddings_path, output_path, words):
        vectors = Cutter.__load_vector(embeddings_path, words)
        Cutter.__save_vectors(output_path, vectors)

    @staticmethod
    def __load_vector(path, words):
        model, is_fast_text_format = Cutter.__load_model(path)
        vectors = {}
        for word in words:
            vector = None
            if word not in model.wv.vocab and Cutter.__is_multiword(word):
                vector = Cutter.__generate_multiword(word, model)
            elif word in model.wv.vocab or is_fast_text_format:
                try:
                    vector = model.wv[word]
                except:
                    vector = None
            if vector is not None:
                vectors[word] = vector
        return vectors

    @staticmethod
    def __load_model(path):
        fastext_format = path.endswith('.bin')
        model = FastText.load_fasttext_format(path, encoding='ISO-8859-1') if fastext_format \
            else KeyedVectors.load_word2vec_format(path, binary=False)
        return model, fastext_format

    @staticmethod
    def __is_multiword(word):
        return '_' in word or '-' in word

    @staticmethod
    def __generate_multiword(word, model):
        vectors = []
        parts = word.split('_')
        vectors = [model.wv[part] for part in parts if part in model.wv.vocab]
        if len(vectors) != len(parts):
            return None
        return np.average(vectors, axis=0)

    @staticmethod
    def __save_vectors(path, vectors):
        with io.open(path, 'w', encoding='utf-8') as file:
            file.write(str(len(vectors)))
            file.write(' 300\n')
            for word, vector in vectors.items():
                vector_str = ' '.join(str(x) for x in vector)
                file.write('{} {}\n'.format(word, vector_str))
